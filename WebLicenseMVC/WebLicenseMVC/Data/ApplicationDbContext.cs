﻿using Licenses.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

using WebLicenseMVC.Models;

namespace WebLicenseMVC.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Software> Softwares { get; set; }
        public DbSet<License> Licenses { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
    }
}
