﻿using Licenses.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebLicenseMVC.Models
{
    public class SoftwareListModel
    {
        public ICollection<Software> Softwares { get; set; }
    }
}
