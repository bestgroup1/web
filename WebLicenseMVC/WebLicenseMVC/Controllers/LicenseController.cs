﻿using Licenses.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebLicenseMVC.Models;
using WebLicenseMVC.Repos;

namespace WebLicenseMVC.Controllers
{
    [Route("license")]
    public class LicenseController : Controller
    {
        private LicenseRepo _licenseRepo;

        public LicenseController(LicenseRepo licenseRepo)
        {
            this._licenseRepo = licenseRepo;
        }

        // GET: LicenseController
        public ActionResult Index()
        {
            return View();
        }

        // GET: LicenseController/Details/5
        [HttpGet("detail/{id:int}")]
        public ActionResult LicenseDetail([FromRoute] int id) //id 1
        {
            License license = _licenseRepo.GetLicense(id); //license null
            return View("LicenseDetail", license);
        }

        [HttpGet("llist")]
        public ActionResult LicenseList()
        {
            List<License> licenses = _licenseRepo.GetLicenses();
            return View("LicensesList", new LicenseListModel { Licenses = licenses });
        }

        // GET: LicenseController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LicenseController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LicenseController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LicenseController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LicenseController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LicenseController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
