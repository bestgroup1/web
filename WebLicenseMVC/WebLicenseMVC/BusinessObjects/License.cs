﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Licenses.Models
{
    [Table("tbLicense")]
    public class License
    {
        [Key]
        [Column("ID")]
        public int ID { get; set; }

        [Column("LicenseName")]
        public string LicenseName { get; set; }

        [Column("Terms")]
        public string Terms { get; set; }

        public License(int iD, string licenseName, string terms)
        {
            ID = iD;
            LicenseName = licenseName;
            Terms = terms;
        }

        public License()
        {

        }
    }
}
