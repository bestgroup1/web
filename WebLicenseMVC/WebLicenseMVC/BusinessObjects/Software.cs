﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Licenses.Models
{
    [Table("tbSoftware")]
    public class Software
    {
        [Key]
        [Column("ID")]
        public int ID { get; set; }

        [Column("Name")]
        public string Name { get; set; }

        [Column("Provider")]
        public string Provider { get; set; }

        [Column("Version")]
        public int Version { get; set; }

        [Column("ReleaseDate")]
        public DateTime ReleaseDate { get; set; }

        [ForeignKey("LicenseID")]
        public virtual int LicenseID { get; set; }
        //public int LicenseID { get; set; }

        public Software(int iD, string name, string provider, int version, DateTime releaseDate, int licenseID)
        {
            ID = iD;
            Name = name;
            Provider = provider;
            Version = version;
            ReleaseDate = releaseDate;
            LicenseID = licenseID;
        }

        public Software()
        {

        }
    }
}
