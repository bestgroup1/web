﻿using Licenses.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebLicenseMVC.Data;

namespace WebLicenseMVC.Repos
{
    public class LicenseRepo
    {
        private ApplicationDbContext _applicationDbContext;

        public LicenseRepo(ApplicationDbContext context)
        {
            this._applicationDbContext = context;
        }

        //crud

        public void Add(License license)
        {
            _applicationDbContext.Add(license);
            _applicationDbContext.SaveChanges();
        }
        public void Update(License license)
        {
            _applicationDbContext.Update(license);
            _applicationDbContext.SaveChanges();
        }
        public void Remove(License license)
        {
            _applicationDbContext.Remove(license);
            _applicationDbContext.SaveChanges();
        }
        public License GetLicense(int ID)
        {
            return _applicationDbContext.Licenses.Find(ID);
                   //_applicationDbContext.Licenses.Where(x => x.ID == ID).FirstOrDefault(/*x => x.ID == ID*/);
        }
        /*public License GetLicenseByID(int ID)
        {
            return _applicationDbContext.Licenses.First(x => x.ID == ID);
        }*/

        //crud list
        public List<License> GetLicenses()
        {
            return _applicationDbContext.Licenses.ToList();
        }
    }
}
