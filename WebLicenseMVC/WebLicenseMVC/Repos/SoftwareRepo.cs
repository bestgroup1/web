﻿using Licenses.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebLicenseMVC.Data;

namespace WebLicenseMVC.Repos
{
    public class SoftwareRepo
    {
        private ApplicationDbContext _applicationDbContext;

        public SoftwareRepo(ApplicationDbContext context)
        {
            this._applicationDbContext = context;
        }

        public void Add(Software software)
        {
            _applicationDbContext.Softwares.Add(software);
            _applicationDbContext.SaveChanges();
        }
        public void Update(Software software)
        {
            _applicationDbContext.Softwares.Update(software);
            _applicationDbContext.SaveChanges();
        }
        public void Remove(Software software)
        {
            _applicationDbContext.Softwares.Remove(software);
            _applicationDbContext.SaveChanges();
        }
        public Software GetSoftware(int ID)
        {
            return _applicationDbContext.Softwares.FirstOrDefault(x => x.ID == ID);
        }

        public List<Software> GetSoftwares()
        {
            return _applicationDbContext.Softwares.ToList();
        }
    }
}
